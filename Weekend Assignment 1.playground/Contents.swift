import UIKit

public class Node<T> {

    var data: T
    var nextNode: Node<T>?
    weak var previousNode: Node<T>?
    
    init(_ data: T) {
        self.data = data
    }
}

public class DoublyLinkedList {
    var headNode: Node<Any>?
    var tailNode: Node<Any>?
    var first: Node<Any>? {
        get {
            return headNode
        }
    }
    
    var last: Node<Any>? {
        get {
            return tailNode
        }
    }
    var length: Int {
        get {
            var count = 0
            var current = self.headNode
            while current != nil {
                count += 1
                current = current?.nextNode
            }
            return count
        }
    }
    
    func add(data: Any) {
        let newNode = Node(data)
        if let tNode = tailNode {
            newNode.previousNode = tNode
            tailNode?.nextNode = newNode
        } else {
            headNode = newNode
        }
        tailNode = newNode
    }
    
    func node(at index: Int) -> Node<Any>? {
        if index >= 0 {
            var node = headNode
            var count = index
            while node != nil {
                if count == 0 {
                    return node
                }
                count -= 1
                node = node!.nextNode
            }
        }
        return nil
    }
    
    func remove(_ node: Node<Any>) -> Any {
        let previous = node.previousNode
        let next = node.nextNode
        
        if let prev = previous {
            prev.nextNode = next
        } else {
            headNode = next
        }
        next?.previousNode = previous
        
        if next == nil {
            tailNode = previous
        }
        
        node.previousNode = nil
        node.nextNode = nil
        
        return node.data
    }
}
extension Node: CustomStringConvertible {
    public var description: String {
        var text = "["
        text += "\(data)"
        return text + "]"
    }
}
extension DoublyLinkedList: CustomStringConvertible {
    public var description: String {
        var text = "H-"
        var node = headNode
        while node != nil {
            text += "[\(node!.data)]"
            node = node!.nextNode
            if node != nil {
                text += "-"
            }
        }
        return text + "-T"
    }
}

public class Stack {
    
    var items = DoublyLinkedList()
    
    var size: Int {
        get {
            return items.length
        }
    }
    var peek: Node<Any>? {
        get {
            return items.last
        }
    }
    
    func push(_ node: Any) {
        items.add(data: node)
    }
    
    func pop() -> Any {
        
        if peek != nil {
            let removed = items.remove(peek!)
            return removed
        } else {
            return Node("Invalid - stack empty")
        }
    }
}
extension Stack: CustomStringConvertible {
    public var description: String {
        var text = "["
        var node = items.headNode
        while node != nil {
            text += "\(node!.data)"
            node = node!.nextNode
            if node != nil {
                text += ", "
            }
        }
        return text + "]"
    }
}


